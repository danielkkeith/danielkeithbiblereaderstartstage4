package bibleReader.tests;

import static org.junit.Assert.assertArrayEquals;
// If you organize imports, the following import might be removed and you will
// not be able to find certain methods. If you can't find something, copy the
// commented import statement below, paste a copy, and remove the comments.
// Keep this commented one in case you organize imports multiple times.
//
// import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import bibleReader.BibleIO;
import bibleReader.model.ArrayListBible;
import bibleReader.model.Bible;
import bibleReader.model.BibleReaderModel;
import bibleReader.model.BookOfBible;
import bibleReader.model.Reference;
import bibleReader.model.Verse;
import bibleReader.model.VerseList;

/**
 * Tests the getReferencesForPassage() method. These tests assume BibleIO is working an can read in
 * the kjv.atv file.
 * 
 * @author Daniel Keith and Grant Williams, February 28, 2019
 */
public class TestGetReferencesForPassage  {
	@Rule
	public Timeout globalTimeout = new Timeout(1000);
	
	private static VerseList	versesFromFile;
	private BibleReaderModel	model;

	@BeforeClass
	public static void readFile() {
		// Our tests will be based on the KJV version for now.
		File file = new File("kjv.atv");
		// We read the file here so it isn't done before every test.
		versesFromFile = BibleIO.readBible(file);
	}

	@Before
	public void setUp() throws Exception {
		// Make a shallow copy of the verses.
		ArrayList<Verse> copyOfList =versesFromFile.copyVerses();
		// Now make a copy of the VerseList
		VerseList copyOfVerseList = new VerseList(versesFromFile.getVersion(), versesFromFile.getDescription(),
				copyOfList);

		Bible testBible = new ArrayListBible(copyOfVerseList);
		model = new BibleReaderModel();
		model.addBible(testBible);
	}

	public VerseList getVersesForReference(String reference) {
		ArrayList<Reference> list = model.getReferencesForPassage(reference);
		VerseList results = model.getVerses("KJV", list);
		return results;
	}
	
	@Test
	public void testSingleVerse() {
		//Expected Verses
		List<Verse> John_3_16 = versesFromFile.subList(26136, 26137);
		List<Verse> Gen_1_1   = versesFromFile.subList(0, 1);
		List<Verse> Rev_22_21   = versesFromFile.subList(31101, 31102);
		
		//Actual Verses
		VerseList actualJohn = getVersesForReference("John 3 : 16");
		VerseList actualGen = getVersesForReference("Gen 1:1");
		VerseList actualRev = getVersesForReference("Revelation 22:21");
		
		//Tests
		assertArrayEquals(John_3_16.toArray(), actualJohn.toArray());
		assertArrayEquals(Gen_1_1.toArray(), actualGen.toArray());
		assertArrayEquals(Rev_22_21.toArray(), actualRev.toArray());
	}
	
	
	@Test
	public void testMultipleVersesOneChapter() {
		//Expected Verses
		List<Verse> Ecc_3_1to8 = versesFromFile.subList(17360, 17368);
		List<Verse> Josh_24_28to33 = versesFromFile.subList(6504, 6510);
		List<Verse> Psa_23_1to6 = versesFromFile.subList(14236, 14242);
		
		//Actual Verses
		VerseList actualEcc = getVersesForReference(" Ecclesiastes 3 : 1 - 8 ");
		VerseList actualJosh = getVersesForReference("Joshua 24:28-33");
		VerseList actualPsa = getVersesForReference("Psalm 23:1-6");
		
		//Test
		assertArrayEquals(Ecc_3_1to8.toArray(), actualEcc.toArray());
		assertArrayEquals(Josh_24_28to33.toArray(), actualJosh.toArray());
		assertArrayEquals(Psa_23_1to6.toArray(), actualPsa.toArray());
	}
	
	@Test
	public void testWholeChapters() {
		//Expected Verses
		List<Verse> SoS_3 = versesFromFile.subList(17572, 17583);
		List<Verse> Rev_22 = versesFromFile.subList(31081, 31102);
		List<Verse> FirstTim_2to4 = versesFromFile.subList(29717, 29764);
		List<Verse> FirstJohn_2to3 = versesFromFile.subList(30551, 30604);
		
		//Actual Verses
		VerseList actualSoS = getVersesForReference("Song of Solomon 3");
		VerseList actualRev = getVersesForReference("Revelation 22");
		VerseList actualTim = getVersesForReference("1 Tim 2-4");
		VerseList actualJohn = getVersesForReference("1 John 2-3");
		
		//Test
		assertArrayEquals(SoS_3.toArray(), actualSoS.toArray());
		assertArrayEquals(Rev_22.toArray(), actualRev.toArray());
		assertArrayEquals(FirstTim_2to4.toArray(), actualTim.toArray());
		assertArrayEquals(FirstJohn_2to3.toArray(), actualJohn.toArray());
	}
	
	@Test
	public void testMultipleVersesMultipleChapters() {
		//Expected Verses
		List<Verse> Isa_52v13to53v12 = versesFromFile.subList(18709, 18724);
		List<Verse> Mal_3v6to4v6 = versesFromFile.subList(23126, 23145);
		
		//some more interesting formatting
		List<Verse> Eph_5to6v9 = versesFromFile.subList(29305, 29347);
		List<Verse> Heb_11to12v2 = versesFromFile.subList(30173, 30215);
		
		//Actual Verses
		VerseList actualIsa = getVersesForReference("Isa 52:13 - 53:12");
		VerseList actualMal = getVersesForReference("Mal 3:6-4:6");
		VerseList actualEph = getVersesForReference("Ephesians 5-6:9");
		VerseList actualHeb = getVersesForReference("Hebrews 11-12:2");
		
		//Test
		assertArrayEquals(Isa_52v13to53v12.toArray(), actualIsa.toArray());
		assertArrayEquals(Mal_3v6to4v6.toArray(), actualMal.toArray());
		assertArrayEquals(Eph_5to6v9.toArray(), actualEph.toArray());
		assertArrayEquals(Heb_11to12v2.toArray(), actualHeb.toArray());
	}
	
	@Test
	public void testWholeBook() {
		//Expected Verses
		List<Verse> FirstKings = versesFromFile.subList(8718, 9534);
		List<Verse> Philemon = versesFromFile.subList(29939, 29964);
		
		//Actual Verses
		VerseList actualKings = getVersesForReference("1 Kings");
		VerseList actualPhil = getVersesForReference("Philemon");
		
		//Test
		assertArrayEquals(FirstKings.toArray(), actualKings.toArray());
		assertArrayEquals(Philemon.toArray(), actualPhil.toArray());
	}
	
	// We expect all invalid searches to return an empty array list.
	
	@Test
	public void testInvalidBookChapterOrVerse() {
		List<Verse> empty = new ArrayList<Verse>();
		assertEquals(empty.toArray(), getVersesForReference("Jude 2"));
		assertEquals(empty.toArray(), getVersesForReference("Herman 2"));
		assertEquals(empty.toArray(), getVersesForReference("John 3:163"));
		assertEquals(empty.toArray(), getVersesForReference("Mal 13:6-24:7"));
	}
	
	@Test
	public void testInvalidSyntax() {
		List<Verse> empty = new ArrayList<Verse>();
		assertEquals(empty.toArray(), getVersesForReference("1Tim3-2"));
		assertEquals(empty.toArray(), getVersesForReference("Deut :2-3"));
		assertEquals(empty.toArray(), getVersesForReference("Josh 6:4- :6"));
		assertEquals(empty.toArray(), getVersesForReference("Ruth : - :"));
		assertEquals(empty.toArray(), getVersesForReference("2 Sam : 4-7 :"));
		assertEquals(empty.toArray(), getVersesForReference("Ephesians 5:2,4"));
		assertEquals(empty.toArray(), getVersesForReference("John 3;16"));
	}
}
