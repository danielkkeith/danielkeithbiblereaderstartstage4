package bibleReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

import bibleReader.model.ArrayListBible;
import bibleReader.model.Bible;
import bibleReader.model.BookOfBible;
import bibleReader.model.Verse;
import bibleReader.model.VerseList;

/**
 * A utility class that has useful methods to read/write Bibles and Verses.
 * 
 * @author cusack
 */
public class BibleIO {

	/**
	 * Read in a file and create a Bible object from it and return it.
	 * 
	 * @param bibleFile
	 * @return
	 */
	// This method is complete, but it won't work until the methods it uses are
	// implemented.
	public static VerseList readBible(File bibleFile) { // Get the extension of
														// the file
		String name = bibleFile.getName();
		String extension = name.substring(name.lastIndexOf('.') + 1, name.length());

		// Call the read method based on the file type.
		if ("atv".equals(extension.toLowerCase())) {
			return readATV(bibleFile);
		} else if ("xmv".equals(extension.toLowerCase())) {
			return readXMV(bibleFile);
		} else {
			return null;
		}
	}

	/**
	 * Read in a Bible that is saved in the "ATV" format. The format is described
	 * below.
	 * 
	 * @param bibleFile The file containing a Bible with .atv extension.
	 * @return A Bible object constructed from the file bibleFile, or null if there
	 *         was an error reading the file.
	 */
	private static VerseList readATV(File bibleFile) {
		try {
			Scanner sc = null;
			sc = new Scanner(bibleFile);
			String version;
			String description;

			// Find the version and description from the first line

			String firstLine = sc.nextLine();
			if (firstLine.contains(":")) {
				String[] parts = firstLine.split(": ");
				version = parts[0];
				description = parts[1];
			} else if (!firstLine.equals("")) {
				version = firstLine;
				description = "";
			} else {
				version = "unknown";
				description = "";
			}

			// Make a new VerseList to add to
			VerseList listToReturn = new VerseList(version, description);

			// Create the Verses and add them to the VerseList
			while (sc.hasNextLine()) {
				BookOfBible book = null;
				int chapter = 0;
				int verse = 0;
				String text = "";

				String nL = sc.nextLine();
				if (nL.contains(":")) {
					String[] firstSplit = nL.split(":", 2);
					String bookAndChapter = firstSplit[0];
					String verseAndText = firstSplit[1];

					if (bookAndChapter.contains("@")) {
						String[] secondSplit = bookAndChapter.split("@");
						BookOfBible tempBook = BookOfBible.getBookOfBible(secondSplit[0]);
						if (tempBook != null) {
							book = tempBook;
							chapter = Integer.parseInt(secondSplit[1]);

							if (verseAndText.contains("@")) {
								String[] thirdSplit = verseAndText.split("@");
								verse = Integer.parseInt(thirdSplit[0]);
								text = thirdSplit[1];
							} else {
								return null;
							}
						} else {
							return null;
						}
					} else {
						return null;
					}
				} else {
					return null;
				}

				Verse newVerse = new Verse(book, chapter, verse, text);
				listToReturn.add(newVerse);
			}
			sc.close();
			return listToReturn;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Read in the Bible that is stored in the XMV format.
	 * 
	 * @param bibleFile The file containing a Bible with .xmv extension.
	 * @return A Bible object constructed from the file bibleFile, or null if there
	 *         was an error reading the file.
	 */
	private static VerseList readXMV(File bibleFile) {
		// TODO Implement me: Stage 8

		// The XMV is sort of XML-like, but it doesn't have end tags.
		// No description of the file format is given here.
		// You need to look at the file to determine how it should be parsed.
		
		try {
			Scanner sc = null;
			sc = new Scanner(bibleFile);
			String version;
			String description;
			
			
			// Get the Version out of the first line
			
			
			String firstLine = sc.nextLine();
			if (firstLine.contains(":")){
				String[] parts = firstLine.split(": ", 2);
				version = parts[0].split(" ")[1];
				description = parts[1];
			} else {
				return null;
			}
			
			BookOfBible book = null;
			int currentChapter = 0;
			VerseList verseListToReturn = new VerseList(version, description);
			
			while(sc.hasNextLine()) {
				String[] parts = sc.next().split(">");
				
				if (parts[0].toLowerCase().contains("book")) {
					// do book thing
					String bookString = parts[0].split("," , 2)[0].toLowerCase().replaceAll("<book ", "");
					book = book.getBookOfBible(bookString);
				}
				else if (parts[0].toLowerCase().contains("chapter")) {
					// do chapter thing
					String chapterString = parts[0].toLowerCase().replaceAll("<chapter ", "");
					currentChapter = Integer.parseInt(chapterString);
				}
				else if (parts[0].toLowerCase().contains("verse")) {
					// do verse thing
					String verseString = parts[0].toLowerCase().replaceAll("<verse ", "");
					int verseNum = Integer.parseInt(verseString);
					String verseText = parts[1];
					
					Verse verseToAdd = new Verse(book, currentChapter, verseNum, verseText);
					verseListToReturn.add(verseToAdd);
				}
				
			}
			
			return verseListToReturn;
			
		}
		catch (FileNotFoundException e){
			e.printStackTrace();
			return null;
		}

		// TODO Documentation: Stage 8 (Update the Javadoc comment to describe
		// the format of the file.)
	}

	// Note: In the following methods, we should really ensure that the file
	// extension is correct
	// (i.e. it should be ".atv"). However for now we won't worry about it.
	// Hopefully the GUI code
	// will be written in such a way that it will require the extension to be
	// correct if we are
	// concerned about it.

	/**
	 * Write out the Bible in the ATV format.
	 * 
	 * @param file  The file that the Bible should be written to.
	 * @param bible The Bible that will be written to the file.
	 * @throws IOException
	 */
	public static void writeBibleATV(File file, Bible bible) throws IOException {
		writeVersesATV(file, bible.getVersion() + ": " + bible.getTitle(), bible.getAllVerses());
	}

	/**
	 * Write out the given verses in the ATV format, using the description as the
	 * first line of the file.
	 * 
	 * @param file        The file that the Bible should be written to.
	 * @param description The contents that will be placed on the first line of the
	 *                    file, formatted appropriately.
	 * @param verses      The verses that will be written to the file.
	 */
	public static void writeVersesATV(File file, String description, VerseList verses) {
		PrintWriter writer;
		try {
			writer = new PrintWriter(new FileWriter(file));

			// Write the first line
			writer.println(description);
			for (Verse v : verses) {
				// write Book@
				writer.print(v.getReference().getBook() + "@");
				// write chapter:verse@
				writer.print(v.getReference().getChapter() + ":" + v.getReference().getVerse() + "@");
				// write text
				writer.print(v.getText());
				writer.println();
			}
			writer.close();
		} catch (IOException e) {
			System.out.println("Invalid File");
			e.printStackTrace();
		}
		
	}

	/**
	 * Write the string out to the given file. It is presumed that the string is an
	 * HTML rendering of some verses, but really it can be anything.
	 * 
	 * @param file
	 * @param text
	 */
	public static void writeText(File file, String text) {
		PrintWriter writer;
		try {
			writer = new PrintWriter(new FileWriter(file));
			writer.print(text);
			writer.close();
		} catch (IOException e) {
			System.out.println("Invalid File");
			e.printStackTrace();
		}
	}
}
