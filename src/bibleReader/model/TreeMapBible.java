package bibleReader.model;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * A class that stores a version of the Bible.
 * 
 * @author Chuck Cusack (Provided the interface)
 * @author ? (provided the implementation)
 */
public class TreeMapBible implements Bible {

	// The Fields
	private String version;
	private String title;
	private TreeMap<Reference, Verse> theVerses;

	/**
	 * Create a new Bible with the given verses.
	 * 
	 * @param version
	 *            the version of the Bible (e.g. ESV, KJV, ASV, NIV).
	 * @param verses
	 *            All of the verses of this version of the Bible.
	 */
	public TreeMapBible(VerseList verses) {
		version = verses.getVersion();
		title = verses.getDescription();
		theVerses = new TreeMap<Reference, Verse>();

		ArrayList<Verse> entries = verses.copyVerses();
		for (Verse v : entries) {
			theVerses.put(v.getReference(), v);
		}
	}

	/**
	 * Returns the number of verses that this Bible is currently storing.
	 * 
	 * @return the number of verses in the Bible.
	 */
	@Override
	public int getNumberOfVerses() {
		return theVerses.size();
	}

	/**
	 * @return a VerseList containing all of the verses from the Bible, in
	 *         order. The version of the VerseList should be set to the version
	 *         of this Bible and the description should be set to the title of
	 *         this Bible.
	 */
	@Override
	public VerseList getAllVerses() {
		Set<Map.Entry<Reference, Verse>> mySet = theVerses.entrySet();
		VerseList versesToReturn = new VerseList(version, title, new ArrayList<Verse>());
		for (Map.Entry<Reference, Verse> mapping : mySet) {
			versesToReturn.add(mapping.getValue());
		}
		return versesToReturn;
	}

	/**
	 * Returns which version this object is storing (e.g. ESV, KJV)
	 * 
	 * @return a string representation of the version.
	 */
	@Override
	public String getVersion() {
		return version;
	}

	/**
	 * @return a string representation of the full title.
	 */
	@Override
	public String getTitle() {
		return title;
	}

	/**
	 * @param ref
	 *            any reference object
	 * @return true if and only if ref is actually in this Bible
	 */
	@Override
	public boolean isValid(Reference ref) {
		return theVerses.containsKey(ref);
	}

	/**
	 * Return the text of the verse with the given reference
	 * 
	 * @param r
	 *            the Reference to the desired verse.
	 * @return a string representation of the text of the verse, or null if the
	 *         Bible does not contain the verse.
	 */
	@Override
	public String getVerseText(Reference r) {
		if (theVerses.containsKey(r)) {
			return theVerses.get(r).getText();
		}
		return null;
	}

	/**
	 * Return a Verse object for the corresponding Reference.
	 * 
	 * @param r
	 *            A reference to the desired verse.
	 * @return a Verse object which has Reference r, or null if the verse isn't
	 *         in the Bible.
	 */
	@Override
	public Verse getVerse(Reference r) {
		return theVerses.get(r);
	}

	/**
	 * @param book
	 *            the book of the Bible
	 * @param chapter
	 *            the chapter of the book
	 * @param verse1
	 *            the first verse of the chapter
	 * @return a Verse object with reference "book chapter:verse", or null if
	 *         the verse isn't in the Bible.
	 */
	@Override
	public Verse getVerse(BookOfBible book, int chapter, int verse) {
		Reference ref = new Reference(book, chapter, verse);
		return getVerse(ref);
	}
	
	/**
	 * Returns a VerseList of all verses containing <i>phrase</i>, which may be
	 * a word, sentence, or whatever. This method just does simple string
	 * matching, so if <i>phrase</i> is <i>eaten</i>, verses with <i>beaten</i>
	 * will be included.
	 * 
	 * @param phrase
	 *            the word/phrase to search for.
	 * @return a VerseList of all verses containing <i>phrase</i>, which may be
	 *         a word, sentence, or whatever. If there are no such verses,
	 *         returns an empty VerseList. In all cases, the version will be set
	 *         to the version of the Bible (via getVersion()) and the
	 *         description will be set to parameter <i>phrase</i>.
	 */
	@Override
	public VerseList getVersesContaining(String phrase) {
		VerseList verseListToReturn = new VerseList(version, phrase);
		if (phrase.equals("")) {
			return verseListToReturn;
		}

		String betterPhrase = phrase.toLowerCase();
		Set<Map.Entry<Reference, Verse>> mySet = theVerses.entrySet();
		for (Map.Entry<Reference, Verse> mapping : mySet) {
			if (mapping.getValue().getText().toLowerCase().contains(betterPhrase)) {
				verseListToReturn.add(mapping.getValue());
			}
		}
		return verseListToReturn;
	}

	/**
	 * Returns a ArrayList<Reference> of all references for verses containing
	 * <i>phrase</i>, which may be a word, sentence, or whatever. This method
	 * just does simple string matching, so if <i>phrase</i> is <i>eaten</i>,
	 * verses with <i>beaten</i> will be included.
	 * 
	 * @param phrase
	 *            the phrase to search for
	 * @return a ArrayList<Reference> of all references for verses containing
	 *         <i>phrase</i>, which may be a word, sentence, or whatever. If
	 *         there are no such verses, returns an empty ArrayList<Reference>.
	 */
	@Override
	public ArrayList<Reference> getReferencesContaining(String phrase) {
		ArrayList<Reference> refsToReturn = new ArrayList<Reference>();
		if (phrase.equals("")) {
			return refsToReturn;
		}

		String betterPhrase = phrase.toLowerCase();
		Set<Map.Entry<Reference, Verse>> mySet = theVerses.entrySet();
		for (Map.Entry<Reference, Verse> mapping : mySet) {
			if (mapping.getValue().getText().toLowerCase().contains(betterPhrase)) {
				refsToReturn.add(mapping.getKey());
			}
		}
		return refsToReturn;
	}

	/**
	 * @param references
	 *            a ArrayList<Reference> of references for which verses are
	 *            being requested
	 * @return a VerseList with each element being the Verse with that Reference
	 *         from this Bible, or null if the particular Reference does not
	 *         occur in this Bible. Thus, the size of the returned list will be
	 *         the same as the size of the references parameter, with the items
	 *         from each corresponding. The version will be set to the version
	 *         of the Bible (via getVersion()) and the description will be set
	 *         "Arbitrary list of Verses".
	 */
	@Override
	public VerseList getVerses(ArrayList<Reference> references) {
		VerseList verseListToReturn = new VerseList(getVersion(), "Arbitrary list of Verses");
		for (Reference r : references) {
			if (!(r.getBookOfBible() == null)){
				verseListToReturn.add(getVerse(r));
			} else{
				verseListToReturn.add(null);
			}
		}
		return verseListToReturn;
	}
	
	/**
	 * @param book
	 *            The book.
	 * @param chapter
	 *            The chapter.
	 * @return the number of the final verse of the given chapter in the given
	 *         book, or -1 if there is a problem (e.g., the book or chapter are
	 *         invalid/null).
	 */
	@Override
	public int getLastVerseNumber(BookOfBible book, int chapter) {
		VerseList verses = new VerseList(getChapter(book, chapter));
		if (book != null && verses.size() > 0) {
			return verses.size();
		}
		return -1;
	}

	/**
	 * @param book
	 *            The book
	 * @return the number of the final chapter of the given book, or -1 if there
	 *         is a problem (e.g., the book is invalid/null).
	 */
	@Override
	public int getLastChapterNumber(BookOfBible book) {
		VerseList verses = new VerseList(getBook(book));
		BookOfBible[] theList = BookOfBible.values();

		for (BookOfBible b : theList) {
			if (book == b) {
				return verses.get(verses.size() - 1).getReference().getChapter();
			}
		}
		return -1;
	}

	/**
	 * @param firstVerse
	 *            the starting verse of the passage
	 * @param lastVerse
	 *            the final verse of the passage
	 * @return a ArrayList<Reference> of all references in this Bible between the
	 *         firstVerse and lastVerse, inclusive of both; or an empty list if
	 *         the range of verses is invalid in this Bible. An invalid range
	 *         includes if either firstVerse or lastVerse is invalid or if
	 *         firstVerse.compareTo(lastVerse) > 0.
	 */
	@Override
	public ArrayList<Reference> getReferencesInclusive(Reference firstVerse, Reference lastVerse) {
		ArrayList<Reference> references = new ArrayList<Reference>();

		if (firstVerse.compareTo(lastVerse) > 0) {
			return references;
		}

		Map<Reference, Verse> s = theVerses.subMap(firstVerse, true, lastVerse, true);
		Set<Map.Entry<Reference, Verse>> mySet = s.entrySet();
		for (Map.Entry<Reference, Verse> element : mySet) {
			Reference aRef = element.getKey();
			references.add(aRef);
		}

		return references;
	}

	/**
	 * @param firstVerse
	 *            the starting verse of the passage
	 * @param lastVerse
	 *            the final verse of the passage
	 * @return a ArrayList<Reference> of all references between the firstVerse and
	 *         lastVerse, exclusive of lastVerse; or an empty list if the range
	 *         of verses is invalid in this Bible.  An invalid range includes if
	 *         firstVerse is invalid or if firstVerse.compareTo(lastVerse) > 0.
	 *         Notice that lastVerse is allowed to be invalid. This can be
	 *         useful in certain cases to find passages more quickly.
	 */
	@Override
	public ArrayList<Reference> getReferencesExclusive(Reference firstVerse, Reference lastVerse) {
		ArrayList<Reference> references = new ArrayList<Reference>();

		if (firstVerse.compareTo(lastVerse) > 0) {
			return references;
		}

		Map<Reference, Verse> s = theVerses.subMap(firstVerse, lastVerse);
		Set<Map.Entry<Reference, Verse>> mySet = s.entrySet();
		for (Map.Entry<Reference, Verse> element : mySet) {
			Reference aRef = element.getKey();
			references.add(aRef);
		}

		return references;
	}

	/**
	 * Return a list of all verses from a given book.
	 * 
	 * @param book
	 *            The desired book.
	 * @return a ArrayList<Reference> with version set to this.getVersion(),
	 *         description set to a String representation of <i>book</i>, and
	 *         containing all verses from the given book, in order; or an empty
	 *         list if the book is invalid in this Bible.
	 */
	@Override
	public ArrayList<Reference> getReferencesForBook(BookOfBible book) {
		ArrayList<Reference> references = new ArrayList<Reference>();

		if (book == null) {
			return references;
		}

		int lastChap = getLastChapterNumber(book);
		int lastVerse = getLastVerseNumber(book, lastChap);

		Reference ref1 = new Reference(book, 1, 1);
		Reference ref2 = new Reference(book, lastChap, lastVerse);

		Map<Reference, Verse> s = theVerses.subMap(ref1, true, ref2, true);
		Set<Map.Entry<Reference, Verse>> mySet = s.entrySet();
		for (Map.Entry<Reference, Verse> element : mySet) {
			Reference aRef = element.getKey();
			references.add(aRef);
		}

		return references;
	}

	/**
	 * @param book
	 *            the book of the Bible
	 * @param chapter
	 *            the chapter of the book
	 * @return a ArrayList<Reference> containing all references from the given chapter
	 *         of the given book, in order; or an empty list if the passage is
	 *         invalid in this Bible.
	 */
	@Override
	public ArrayList<Reference> getReferencesForChapter(BookOfBible book, int chapter) {
		ArrayList<Reference> references = new ArrayList<Reference>();

		int lastVerse = getLastVerseNumber(book, chapter);

		Reference ref1 = new Reference(book, chapter, 1);
		Reference ref2 = new Reference(book, chapter, lastVerse);

		if (!isValid(ref1) || !isValid(ref2)) {
			return references;
		} else if (ref1.compareTo(ref2) > 0) {
			return references;
		}

		Map<Reference, Verse> s = theVerses.subMap(ref1, true, ref2, true);
		Set<Map.Entry<Reference, Verse>> mySet = s.entrySet();
		for (Map.Entry<Reference, Verse> element : mySet) {
			Reference aRef = element.getKey();
			references.add(aRef);
		}

		return references;
	}

	/**
	 * @param book
	 *            the book of the Bible
	 * @param chapter1
	 *            the chapter of the book
	 * @param chapter2
	 *            the chapter of the book
	 * @return a ArrayList<Reference> containing all references from the given chapters
	 *         of the given book, in order; or an empty list if the passage is
	 *         invalid in this Bible.
	 */
	@Override
	public ArrayList<Reference> getReferencesForChapters(BookOfBible book, int chapter1, int chapter2) {
		ArrayList<Reference> references = new ArrayList<Reference>();

		int lastVerse = getLastVerseNumber(book, chapter2);

		Reference ref1 = new Reference(book, chapter1, 1);
		Reference ref2 = new Reference(book, chapter2, lastVerse);

		if (!isValid(ref1) || !isValid(ref2)) {
			return references;
		} else if (ref1.compareTo(ref2) > 0) {
			return references;
		}

		Map<Reference, Verse> s = theVerses.subMap(ref1, true, ref2, true);
		Set<Map.Entry<Reference, Verse>> mySet = s.entrySet();
		for (Map.Entry<Reference, Verse> element : mySet) {
			Reference aRef = element.getKey();
			references.add(aRef);
		}

		return references;
	}

	/**
	 * @param book
	 *            the book of the Bible
	 * @param chapter
	 *            the chapter of the book
	 * @param verse1
	 *            the first verse of the chapter
	 * @param verse2
	 *            the second verse of the chapter
	 * @return a ArrayList<Reference> containing all references from the given passage,
	 *         in order; or an empty list if the passage is invalid in this
	 *         Bible.
	 */
	@Override
	public ArrayList<Reference> getReferencesForPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		ArrayList<Reference> references = new ArrayList<Reference>();
		Reference ref1 = new Reference(book, chapter, verse1);
		Reference ref2 = new Reference(book, chapter, verse2);

		if (!isValid(ref1) || !isValid(ref2)) {
			return references;
		} else if (ref1.compareTo(ref2) > 0) {
			return references;
		}

		Map<Reference, Verse> s = theVerses.subMap(ref1, true, ref2, true);
		Set<Map.Entry<Reference, Verse>> mySet = s.entrySet();
		for (Map.Entry<Reference, Verse> element : mySet) {
			Reference aRef = element.getKey();
			references.add(aRef);
		}
		return references;
	}

	/**
	 * @param book
	 *            the book of the Bible
	 * @param chapter1
	 *            the first chapter of the book
	 * @param verse1
	 *            the first verse of the chapter
	 * @param chapter2
	 *            the second chapter of the book
	 * @param verse2
	 *            the second verse of the chapter
	 * @return a ArrayList<Reference> containing all references from the given passage,
	 *         in order; or an empty list if the passage is invalid in this
	 *         Bible.
	 */
	@Override
	public ArrayList<Reference> getReferencesForPassage(BookOfBible book, int chapter1, int verse1, int chapter2,
			int verse2) {
		ArrayList<Reference> references = new ArrayList<Reference>();
		Reference ref1 = new Reference(book, chapter1, verse1);
		Reference ref2 = new Reference(book, chapter2, verse2);

		if (!isValid(ref1) || !isValid(ref2)) {
			return references;
		}

		Map<Reference, Verse> s = theVerses.subMap(ref1, true, ref2, true);
		Set<Map.Entry<Reference, Verse>> mySet = s.entrySet();
		for (Map.Entry<Reference, Verse> element : mySet) {
			Reference aRef = element.getKey();
			references.add(aRef);
		}
		return references;
	}

	/**
	 * @param firstVerse
	 *            the starting verse of the passage
	 * @param lastVerse
	 *            the final verse of the passage
	 * @return a VerseList with version set to this.getVersion(), description
	 *         set to a String representation of the range of verses requested,
	 *         and containing all verses between the firstVerse and lastVerse,
	 *         inclusive of both; or an empty list if the range of verses is
	 *         invalid in this Bible. An invalid range includes if either
	 *         firstVerse or lastVerse is invalid or if
	 *         firstVerse.compareTo(lastVerse) > 0.
	 */
	@Override
	public VerseList getVersesInclusive(Reference firstVerse, Reference lastVerse) {
		VerseList verses = new VerseList(this.getVersion(), firstVerse + "-" + lastVerse);

		if (firstVerse.compareTo(lastVerse) > 0) {
			return verses;
		}

		Map<Reference, Verse> s = theVerses.subMap(firstVerse, true, lastVerse, true);
		Set<Map.Entry<Reference, Verse>> mySet = s.entrySet();
		for (Map.Entry<Reference, Verse> element : mySet) {
			Verse aVerse = new Verse(element.getKey(), element.getValue().getText());
			verses.add(aVerse);
		}

		return verses;
	}

	/**
	 * @param firstVerse
	 *            the starting verse of the passage
	 * @param lastVerse
	 *            the final verse of the passage
	 * @return a VerseList with version set to this.getVersion(), description
	 *         set to a String representation of the range of verses requested,
	 *         with " excluding the final one" tacked onto the end, and
	 *         containing all verses between the firstVerse and lastVerse,
	 *         exclusive of the last one; or an empty list if the range of
	 *         verses is invalid in this Bible. An invalid range includes if
	 *         firstVerse is invalid or if firstVerse.compareTo(lastVerse) > 0.
	 *         Notice that lastVerse is allowed to be invalid. This can be
	 *         useful in certain cases to find passages more quickly.
	 */
	@Override
	public VerseList getVersesExclusive(Reference firstVerse, Reference lastVerse) {
		// Implementation of this method provided by Chuck Cusack.
		VerseList someVerses = new VerseList(getVersion(), firstVerse + "-" + lastVerse);
		
		if (firstVerse.compareTo(lastVerse) > 0) {
			return someVerses;
		}
		
		SortedMap<Reference, Verse> s = theVerses.subMap(firstVerse, lastVerse);
		Set<Map.Entry<Reference, Verse>> mySet = s.entrySet();
		for (Map.Entry<Reference, Verse> element : mySet) {
			Verse aVerse = new Verse(element.getKey(), element.getValue().getText());
			someVerses.add(aVerse);
		}
		return someVerses;
	}

	/**
	 * Return a list of all verses from a given book.
	 * 
	 * @param book
	 *            The desired book.
	 * @return a VerseList with version set to this.getVersion(), description
	 *         set to a String representation of <i>book</i>, and containing all
	 *         verses from the given book, in order; or an empty list if the
	 *         book is invalid in this Bible.
	 */
	@Override
	public VerseList getBook(BookOfBible book) {
		if (book != null) {
			VerseList verses = new VerseList(getVersion(), book.toString());
			VerseList allVerses = new VerseList(getAllVerses());

			for (Verse v : allVerses) {
				if (v.getReference().getBookOfBible() == book) {
					verses.add(v);
				}
			}
			return verses;
		} else {
			VerseList emptyList = new VerseList("", "");
			return emptyList;
		}
	}

	/**
	 * @param book
	 *            the book of the Bible
	 * @param chapter
	 *            the chapter of the book
	 * @return a VerseList with version set to this.getVersion(), description
	 *         set to a String representing the book followed by the chapter
	 *         (e.g. "Genesis 1"), and containing all verses from the given
	 *         chapter of the given book, in order; or an empty list if the book
	 *         is invalid in this Bible.
	 */
	@Override
	public VerseList getChapter(BookOfBible book, int chapter) {
		if (book != null && isValid(new Reference(book, chapter, 1))) {
			VerseList verses = new VerseList(this.getVersion(), book.toString() + " " + chapter);
			VerseList allVerses = new VerseList(getAllVerses());

			for (Verse v : allVerses) {
				if (v.getReference().getChapter() == chapter && v.getReference().getBookOfBible() == book) {
					verses.add(v);
				}
			}
			return verses;
		} else {
			VerseList emptyList = new VerseList("", "");
			return emptyList;
		}
	}

	/**
	 * @param book
	 *            the book of the Bible
	 * @param chapter1
	 *            the chapter of the book
	 * @param chapter2
	 *            the chapter of the book
	 * @return a VerseList with version set to this.getVersion(), description
	 *         set to a String representing the book followed by the chapters
	 *         (e.g. "Genesis 1-2"), and containing all verses from the given
	 *         chapters of the given book, in order; or an empty list if the
	 *         passage is invalid in this Bible.
	 */
	@Override
	public VerseList getChapters(BookOfBible book, int chapter1, int chapter2) {
		VerseList verses = new VerseList(this.getVersion(), book.toString() + " " + chapter2);
		int lastVerse = getLastVerseNumber(book, chapter2);

		Reference ref1 = new Reference(book, chapter1, 1);
		Reference ref2 = new Reference(book, chapter2, lastVerse);

		if (!isValid(ref1) || !isValid(ref2)) {
			return verses;
		} else if (ref1.compareTo(ref2) > 0) {
			return verses;
		}

		Map<Reference, Verse> s = theVerses.subMap(ref1, true, ref2, true);
		Set<Map.Entry<Reference, Verse>> mySet = s.entrySet();
		for (Map.Entry<Reference, Verse> element : mySet) {
			Verse aVerse = new Verse(element.getKey(), element.getValue().getText());
			verses.add(aVerse);
		}
		return verses;
	}

	/**
	 * @param book
	 *            the book of the Bible
	 * @param chapter
	 *            the chapter of the book
	 * @param verse1
	 *            the first verse of the chapter
	 * @param verse2
	 *            the second verse of the chapter
	 * @return a VerseList with version set to this.getVersion(), description
	 *         set to a String representing the verses requested (e.g.
	 *         "Genesis 1:3-17"), and containing all verses from the given
	 *         passage, in order; or an empty list if the passage is invalid in
	 *         this Bible.
	 */
	@Override
	public VerseList getPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		VerseList passages = new VerseList(this.getVersion(),
				book.toString() + " " + chapter + ":" + verse1 + "-" + verse2);

		Reference ref1 = new Reference(book, chapter, verse1);
		Reference ref2 = new Reference(book, chapter, verse2);

		if (!isValid(ref1) || !isValid(ref2)) {
			return passages;
		} else if (ref1.compareTo(ref2) > 0) {
			return passages;
		}

		Map<Reference, Verse> s = theVerses.subMap(ref1, true, ref2, true);
		Set<Map.Entry<Reference, Verse>> mySet = s.entrySet();
		for (Map.Entry<Reference, Verse> element : mySet) {
			Verse aVerse = new Verse(element.getKey(), element.getValue().getText());
			passages.add(aVerse);
		}
		return passages;
	}

	/**
	 * @param book
	 *            the book of the Bible
	 * @param chapter1
	 *            the first chapter of the book
	 * @param verse1
	 *            the first verse of the chapter
	 * @param chapter2
	 *            the second chapter of the book
	 * @param verse2
	 *            the second verse of the chapter
	 * @return a VerseList with version set to this.getVersion(), description
	 *         set to a String representing the verses requested (e.g. "Genesis
	 *         1:3-2:17"), and containing all verses from the given passage, in
	 *         order; or an empty list if the passage is invalid in this Bible.
	 */
	@Override
	public VerseList getPassage(BookOfBible book, int chapter1, int verse1, int chapter2, int verse2) {
		VerseList passages = new VerseList(this.getVersion(),
				book.toString() + " " + chapter1 + ":" + verse1 + "-" + chapter2 + ":" + verse2);
		Reference ref1 = new Reference(book, chapter1, verse1);
		Reference ref2 = new Reference(book, chapter2, verse2);

		if (!isValid(ref1) || !isValid(ref2)) {
			return passages;
		} else if (ref1.compareTo(ref2) > 0) {
			return passages;
		}

		Map<Reference, Verse> s = theVerses.subMap(ref1, true, ref2, true);
		Set<Map.Entry<Reference, Verse>> mySet = s.entrySet();
		for (Map.Entry<Reference, Verse> element : mySet) {
			Verse aVerse = new Verse(element.getKey(), element.getValue().getText());
			passages.add(aVerse);
		}
		return passages;
	}

}
