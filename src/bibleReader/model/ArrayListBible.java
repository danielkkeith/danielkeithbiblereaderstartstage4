package bibleReader.model;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * A class that stores a version of the Bible.
 * 
 * @author Chuck Cusack (Provided the interface). Modified February 9, 2015.
 * @author Daniel Keith (provided the implementation). February 13, 2019.
 */
public class ArrayListBible implements Bible {

	private ArrayList<Verse> bible;
	private String version;
	private String title;

	/**
	 * Create a new Bible with the given verses.
	 * 
	 * @param verses
	 *            All of the verses of this version of the Bible.
	 */
	public ArrayListBible(VerseList verses) {
		bible = verses.copyVerses();
		version = verses.getVersion();
		title = verses.getDescription();
	}

	@Override
	public int getNumberOfVerses() {
		return bible.size();
	}

	@Override
	public String getVersion() {
		return version;
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public boolean isValid(Reference ref) {
		Iterator<Verse> it = bible.iterator();
		while (it.hasNext()) {
			Verse v = it.next();
			if (ref.equals(v.getReference())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String getVerseText(Reference r) {
		Iterator<Verse> it = bible.iterator();
		while (it.hasNext()) {
			Verse v = it.next();
			if (r.equals(v.getReference())) {
				return v.getText();
			}
		}
		return null;
	}

	@Override
	public Verse getVerse(Reference r) {
		for (Verse v : bible) {
			if (v.getReference().equals(r)) {
				return v;
			}
		}
		return null;
	}

	@Override
	public Verse getVerse(BookOfBible book, int chapter, int verse) {
		Reference ref = new Reference(book, chapter, verse);
		Iterator<Verse> it = bible.iterator();
		while (it.hasNext()) {
			Verse v = it.next();
			if (ref.equals(v.getReference())) {
				return v;
			}
		}
		return null;
	}

	/**
	 * @return a VerseList containing all of the verses from the Bible, in
	 *         order. The version of the VerseList should be set to the version
	 *         of this Bible and the description should be set to the title of
	 *         this Bible.
	 */
	@Override
	public VerseList getAllVerses() {
		VerseList verseListToReturn = new VerseList(version, title, bible);
		return verseListToReturn;
	}

	/**
	 * Returns a VerseList of all verses containing <i>phrase</i>, which may be
	 * a word, sentence, or whatever. This method just does simple string
	 * matching, so if <i>phrase</i> is <i>eaten</i>, verses with <i>beaten</i>
	 * will be included.
	 * 
	 * @param phrase
	 *            the word/phrase to search for.
	 * @return a VerseList of all verses containing <i>phrase</i>, which may be
	 *         a word, sentence, or whatever. If there are no such verses,
	 *         returns an empty VerseList. In all cases, the version will be set
	 *         to the version of the Bible (via getVersion()) and the
	 *         description will be set to parameter <i>phrase</i>.
	 */
	@Override
	public VerseList getVersesContaining(String phrase) {
		VerseList verseListToReturn = new VerseList(version, phrase);
		if (phrase.equals("")) {
			return verseListToReturn;
		}

		String betterPhrase = phrase.toLowerCase();

		for (Verse v : bible) {
			if (v.getText().toLowerCase().contains(betterPhrase)) {
				verseListToReturn.add(v);
			}
		}
		return verseListToReturn;
	}

	@Override
	public ArrayList<Reference> getReferencesContaining(String phrase) {
		ArrayList<Reference> refsToReturn = new ArrayList<Reference>();
		if (phrase.equals("")) {
			return refsToReturn;
		}

		String betterPhrase = phrase.toLowerCase();

		for (Verse v : bible) {
			if (v.getText().toLowerCase().contains(betterPhrase)) {
				refsToReturn.add(v.getReference());
			}
		}
		return refsToReturn;
	}

	/**
	 * @param references
	 *            a ArrayList<Reference> of references for which verses are
	 *            being requested
	 * @return a VerseList with each element being the Verse with that Reference
	 *         from this Bible, or null if the particular Reference does not
	 *         occur in this Bible. Thus, the size of the returned list will be
	 *         the same as the size of the references parameter, with the items
	 *         from each corresponding. The version will be set to the version
	 *         of the Bible (via getVersion()) and the description will be set
	 *         "Arbitrary list of Verses".
	 */
	@Override
	public VerseList getVerses(ArrayList<Reference> references) {
		VerseList verseListToReturn = new VerseList(getVersion(), "Arbitrary list of Verses");
		for (Reference r : references) {
			verseListToReturn.add(getVerse(r));
		}
		return verseListToReturn;
	}
	// ---------------------------------------------------------------------------------------------
	// The following part of this class should be implemented for Stage 7.
	//
	// HINT: Do not reinvent the wheel. Some of these methods can be implemented
	// by looking up
	// one or two things and calling another method to do the bulk of the work.
	// ---------------------------------------------------------------------------------------------

	@Override
	public int getLastVerseNumber(BookOfBible book, int chapter) {
		VerseList verses = new VerseList(getChapter(book, chapter));
		if (book != null && verses.size() > 0) {
			return verses.size() - 1;
		}
		return -1;
	}

	@Override
	public int getLastChapterNumber(BookOfBible book) {
		VerseList verses = new VerseList(getBook(book));
		BookOfBible[] theList = BookOfBible.values();

		for (BookOfBible b : theList) {
			if (book == b) {
				return verses.get(verses.size() - 1).getReference().getChapter();
			}
		}
		return -1;
	}

	@Override
	public ArrayList<Reference> getReferencesInclusive(Reference firstVerse, Reference lastVerse) {
		ArrayList<Reference> refs = new ArrayList<Reference>();
		VerseList verses = new VerseList(getAllVerses());

		if (isValid(firstVerse) && isValid(lastVerse)) {
			for (Verse v : verses) {
				if (v.getReference().compareTo(firstVerse) >= 0 && v.getReference().compareTo(lastVerse) <= 0) {
					refs.add(v.getReference());
				}
			}
		}
		return refs;
	}

	@Override
	public ArrayList<Reference> getReferencesExclusive(Reference firstVerse, Reference lastVerse) {
		ArrayList<Reference> refs = new ArrayList<Reference>();
		VerseList verses = new VerseList(getAllVerses());

		if (isValid(firstVerse) && isValid(lastVerse)) {
			if (firstVerse.compareTo(lastVerse) < 0) {
				for (Verse v : verses) {
					if (v.getReference().compareTo(firstVerse) >= 0 && v.getReference().compareTo(lastVerse) < 0) {
						refs.add(v.getReference());
					}
				}
			}
		}
		return refs;
	}

	@Override
	public ArrayList<Reference> getReferencesForBook(BookOfBible book) {
		ArrayList<Reference> refs = new ArrayList<Reference>();
		VerseList verses = new VerseList(getAllVerses());

		for (Verse v : verses) {
			if (v.getReference().getBookOfBible() == book) {
				refs.add(v.getReference());
			}
		}

		return refs;
	}

	@Override
	public ArrayList<Reference> getReferencesForChapter(BookOfBible book, int chapter) {
		ArrayList<Reference> refs = new ArrayList<Reference>();
		VerseList verses = new VerseList(getAllVerses());

		for (Verse v : verses) {
			if (v.getReference().getBookOfBible() == book && v.getReference().getChapter() == chapter) {
				refs.add(v.getReference());
			}
		}

		return refs;
	}

	@Override
	public ArrayList<Reference> getReferencesForChapters(BookOfBible book, int chapter1, int chapter2) {
		ArrayList<Reference> refs = new ArrayList<Reference>();
		VerseList verses = new VerseList(getAllVerses());

		for (Verse v : verses) {
			if (v.getReference().getBookOfBible() == book) {
				if (v.getReference().getChapter() >= chapter1 && v.getReference().getChapter() <= chapter2)
					refs.add(v.getReference());
			}
		}

		return refs;

	}

	@Override
	public ArrayList<Reference> getReferencesForPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		ArrayList<Reference> refs = new ArrayList<Reference>();
		VerseList verses = new VerseList(getAllVerses());

		for (Verse v : verses) {
			if (v.getReference().getBookOfBible() == book && v.getReference().getChapter() == chapter) {
				if (v.getReference().getChapter() >= verse1 && v.getReference().getChapter() <= verse2)
					refs.add(v.getReference());
			}
		}

		return refs;
	}

	@Override
	public ArrayList<Reference> getReferencesForPassage(BookOfBible book, int chapter1, int verse1, int chapter2,
			int verse2) {

		ArrayList<Reference> tempRefs = new ArrayList<Reference>();
		tempRefs = getReferencesForChapters(book, chapter1, chapter2);
		for (int i = tempRefs.size() - 1; i >= 0; i--) {
			if (tempRefs.get(i).getVerse() < verse1 && tempRefs.get(i).getChapter() == chapter1) {
				tempRefs.remove(i);
			}
			if (tempRefs.get(i).getVerse() > verse1 && tempRefs.get(i).getChapter() == chapter1) {
				tempRefs.remove(i);
			}

		}

		return tempRefs;
	}

	@Override
	public VerseList getVersesInclusive(Reference firstVerse, Reference lastVerse) {
		VerseList verses = new VerseList(this.getVersion(), firstVerse + "-" + lastVerse);
		VerseList allVerses = new VerseList(getAllVerses());

		if (isValid(firstVerse) && isValid(lastVerse)) {
			if (firstVerse.compareTo(lastVerse) < 0) {
				for (Verse v : allVerses) {
					if (v.getReference().compareTo(firstVerse) >= 0 && v.getReference().compareTo(lastVerse) <= 0) {
						verses.add(v);
					}
				}
			}
		}

		return verses;
	}

	@Override
	public VerseList getVersesExclusive(Reference firstVerse, Reference lastVerse) {
		VerseList verses = new VerseList(this.getVersion(), firstVerse + "-" + lastVerse);
		VerseList allVerses = new VerseList(getAllVerses());

		if (isValid(firstVerse) && isValid(lastVerse)) {
			if (firstVerse.compareTo(lastVerse) < 0) {
				for (Verse v : allVerses) {
					if (v.getReference().compareTo(firstVerse) >= 0 && v.getReference().compareTo(lastVerse) < 0) {
						verses.add(v);
					}
				}
			}
		}

		return verses;
	}

	@Override
	public VerseList getBook(BookOfBible book) {
		if (book != null) {
			VerseList verses = new VerseList(this.getVersion(), book.toString());
			VerseList allVerses = new VerseList(getAllVerses());

			for (Verse v : allVerses) {
				if (v.getReference().getBookOfBible() == book) {
					verses.add(v);
				}
			}
			return verses;
		} else {
			VerseList emptyList = new VerseList("", "");
			return emptyList;
		}
	}

	@Override
	public VerseList getChapter(BookOfBible book, int chapter) {
		if (book != null && isValid(new Reference(book, chapter, 1))) {
			VerseList verses = new VerseList(this.getVersion(), book.toString() + " " + chapter);
			VerseList allVerses = new VerseList(getAllVerses());

			for (Verse v : allVerses) {
				if (v.getReference().getChapter() == chapter && v.getReference().getBookOfBible() == book) {
					verses.add(v);
				}
			}
			return verses;
		} else {
			VerseList emptyList = new VerseList("", "");
			return emptyList;
		}
	}

	@Override
	public VerseList getChapters(BookOfBible book, int chapter1, int chapter2) {
		VerseList verses = new VerseList(this.getVersion(), book.toString() + " " + chapter2);
		VerseList allVerses = new VerseList(getAllVerses());

		if (chapter1 < chapter2) {
			for (Verse v : allVerses) {
				if (v.getReference().getBookOfBible() == book) {
					if (v.getReference().getChapter() >= chapter1 && v.getReference().getChapter() <= chapter2) {
						verses.add(v);
					}
				}
			}
		}
		return verses;
	}

	@Override
	public VerseList getPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		VerseList verses = new VerseList(getChapter(book, chapter));
		VerseList passages = new VerseList(this.getVersion(),
				book.toString() + " " + chapter + ":" + verse1 + "-" + verse2);
		

		if (verse1 < verse2) {
			for (Verse v : verses) {
				Reference ref = v.getReference();
				if (ref.getBookOfBible() == book) {
					if (ref.getChapter() == chapter) {
						if (ref.getVerse() >= verse1 && ref.getVerse() <= verse2)
						passages.add(v);
					}
				}
			}
		}
		return passages;
	}

	@Override
	public VerseList getPassage(BookOfBible book, int chapter1, int verse1, int chapter2, int verse2) {
		VerseList verses = new VerseList(getChapters(book, chapter1, chapter2));
		VerseList passages = new VerseList(this.getVersion(),
				book.toString() + " " + chapter1 + ":" + verse1 + "-" + chapter2 + ":" + verse2);
		

		if (verse1 < verse2) {
			for (Verse v : verses) {
				Reference ref = v.getReference();
				if (ref.getBookOfBible() == book) {
					if (ref.getVerse() >= verse1 && ref.getChapter() == chapter1) {
						passages.add(v);
					} else if (ref.getVerse() <= verse2 && ref.getChapter() == chapter2){
						passages.add(v);
					} else if (ref.getChapter() > chapter1 && ref.getChapter() < chapter2) {
						passages.add(v);
					}
				}
			}
		}
		return passages;
	}
}
